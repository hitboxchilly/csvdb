import argparse
import csv
import datetime
import glob
import os
import pickle
import platform
import sqlite3
import subprocess
import tempfile
import textwrap

from itertools import chain
from pathlib import Path
from pprint import pprint

try:
    import openpyxl
except ImportError:
    openpyxl = None

class CSVDBError(Exception):
    pass

class Result:

    def __init__(self, data, fieldnames):
        self.data = data
        self.fieldnames = fieldnames


def open_with_default(filename):
    """
    Open filename with OS default application.
    """
    if platform.system() == 'Darwin':
        # macOS
        subprocess.run(('open', filename))
    elif platform.system() == 'Windows':
        # Windows
        os.startfile(filename)
    else:
        # linux variants
        subprocess.run(('xdg-open', filename))

def load(
    sources,
    include_filename = False,
    default_filename_fieldname = 'filename',
    sniffer_preferred = None,
):
    # set up pre_add function
    filename_fieldname = default_filename_fieldname
    if include_filename:
        def pre_add(fn, row):
            row[filename_fieldname] = fn
    else:
        def pre_add(fn, row):
            # no op
            pass
    # load csv data
    data = []
    def p(s):
        return glob.glob(os.path.expanduser(s))
    sources = list(chain(*map(p, sources)))
    fieldnames = None
    for source in sources:
        source = Path(source)
        # skip empty files
        stat = os.stat(source)
        if stat.st_size == 0:
            continue
        with open(source, newline='') as src_fp:
            sample = src_fp.read()
            sniffer = csv.Sniffer()
            if sniffer_preferred is not None:
                sniffer.preferred = sniffer_preferred
            dialect = sniffer.sniff(sample)
            src_fp.seek(0)
            csv_reader = csv.DictReader(src_fp, dialect=dialect)
            fieldnames = csv_reader.fieldnames
            if include_filename:
                # add source filename to output
                if filename_fieldname in fieldnames:
                    # unique-ify header fieldname
                    n = 0
                    while filename_fieldname in fieldnames:
                        filename_fieldname = f'{default_filename_fieldname}.{n}'
                        if n > 20:
                            raise CSVDBError(
                                'Unable to find a unique name for filename field.')
                else:
                    filename_fieldname = default_filename_fieldname
                fieldnames.append(filename_fieldname)
            for row in csv_reader:
                pre_add(source.name, row)
                data.append(row)
    result = Result(data, fieldnames)
    return result

def integer(value):
    int(str(value).replace(',', ''))

class IsType:

    def __init__(self, func, conv):
        self.func = func
        self.conv = conv

    def __call__(self, value):
        try:
            self.func(value)
        except:
            rv = False
        else:
            rv = True
        return rv


def smart_int(value):
    if str(value) == '':
        return None
    else:
        return int(str(value).replace(',', ''))

is_float = IsType(float, float)
is_int = IsType(integer, smart_int)

try:
    import dateutil.parser
except ImportError:
    is_date = IsType(lambda value: False, None)
else:
    def smart_parse(value):
        if str(value) == '':
            return None
        else:
            return dateutil.parser.parse(value)
    is_date = IsType(dateutil.parser.parse, smart_parse)

def datatypes(result):
    values = [ [row[fieldname] for fieldname in result.fieldnames] for row in result.data]
    columns = list(zip(*values))

    types = {}
    for fieldname, values in zip(result.fieldnames, columns):
        values = [value for value in values if str(value) != '']
        if all(map(is_int, values)):
            type_ = is_int.conv
        elif all(map(is_float, values)):
            type_ = is_float.conv
        elif all(map(is_date, values)):
            type_ = is_date.conv
        else:
            type_ = str
        types[fieldname] = type_
    return types

def main(argv=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('sources', nargs='+')
    parser.add_argument('--sniffer-preferred', nargs='+')
    parser.add_argument('-t', '--type', action='store_true',
                        help='Try to convert to data types.')
    parser.add_argument('--filename', action='store_true',
                        help='Add filename to data.')
    parser.add_argument('--sqlite', help='Output sqlite file.')
    parser.add_argument('--pickle', help='Output pickle file.')
    if openpyxl is not None:
        parser.add_argument('--xlsx', action='store_true')
    args = parser.parse_args(argv)

    result = load(
        args.sources,
        args.filename,
        sniffer_preferred = args.sniffer_preferred
    )

    if args.type:
        types = datatypes(result)
        for row in result.data:
            for key, value in row.items():
                row[key] = types[key](row[key])

    if 'xlsx' in args and args.xlsx:
        wb = openpyxl.Workbook(write_only=True)
        ws = wb.create_sheet()
        ws.append(result.fieldnames)
        for row in result.data:
            ws.append(list(row.values()))
        with tempfile.NamedTemporaryFile(delete=False, suffix='.xlsx') as dst:
            wb.save(dst.name)
        open_with_default(dst.name)
    elif args.sqlite:
        if Path(args.sqlite).exists():
            raise CSVDBError('%r already exists' % str(args.sqlite))
        with sqlite3.connect(args.sqlite) as conn:
            column_defs = textwrap.indent(',\n'.join(result.fieldnames), ' ' * 4)
            conn.execute(f'CREATE TABLE csvdb ({column_defs}\n);')
            posargs = ', '.join('?' for _ in result.fieldnames)
            sql = f'INSERT INTO csvdb VALUES ({posargs});'
            for row in result.data:
                values = [row[name] for name in result.fieldnames]
                conn.execute(sql, values)
    elif args.pickle:
        if Path(args.pickle).exists():
            raise CSVDBError('%r already exists' % str(args.sqlite))
        with open(args.pickle, 'wb') as dst:
            pickle.dump(result.data, dst)
    else:
        pprint(result.data)

if __name__ == '__main__':
    main()
